package main

import (
	"context"
	"log"
	"time"

	"github.com/jaslife1/uniuri"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/image-service/proto/image"
)

func main() {
	srv := micro.NewService(

		micro.Name("image-service-cli"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Create a new client
	c := pb.NewImageServiceClient("image-service", client.DefaultClient)

	// Get specific image
	{
		id := uniuri.New()

		start := time.Now()

		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		imageID := "5c841f2822d9981351784477"
		resp, err := c.GetImage(ctx, &pb.Image{Id: imageID})

		if err != nil {
			log.Panicf("Error: Did not get Image with ID: %s - %v", imageID, err)
		}

		log.Printf("Image: %v", resp.Image)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}
}
